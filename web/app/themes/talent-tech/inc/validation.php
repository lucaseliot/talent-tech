<?php


function cleanXss($key)
{
    return trim(strip_tags($_POST[$key]));
}

function getPostValue($key)
{
    if(!empty($_POST[$key]) ) {
        echo $_POST[$key];
    }
}

function viewError($err, $key)
{
    if(!empty($err[$key])) {
        echo $err[$key];
    }
}


function validText($err, $value,$keyErr,$min,$max)
{
    if(!empty($value)) {
        if(mb_strlen($value) < $min ) {
            $err[$keyErr] = 'Veuillez renseigner au moins '.$min.' caractères';
        } elseif(mb_strlen($value) > $max ) {
            $err[$keyErr] = 'Veuillez renseigner pas plus de '.$max.' caractères';
        }
    } else {
        $err[$keyErr] = 'Veuillez renseigner ce champ';
    }
    return $err;
}


function validEmail($err,$value, $keyErr)
{
    if(!empty($value)) {
        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            $err[$keyErr] = 'Veuillez renseigner un email valide';
        }
    } else {
        $err[$keyErr] = 'Veuillez renseigner un email';
    }
    return $err;
}

function getPostValue2($key, $data='')
{
    if(!empty($_POST[$key])) {
        echo $_POST[$key];
    }elseif(!empty($data)) {
        echo $data;
    }
}



